import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.jfrog.bintray.gradle.BintrayExtension

group = "shareandcharge.openchargingnetwork"
version = "1.0.1"

plugins {
    `maven-publish`
    kotlin("jvm") version "1.3.61"
    id("org.jetbrains.dokka") version "0.10.0"
    id("com.jfrog.bintray") version "1.8.4"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib:1.3.61"))
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.61")
    implementation("org.web3j:core:4.5.5")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.10.2")
    implementation("com.jayway.jsonpath:json-path:2.4.0")
    implementation("com.nixxcode.jvmbrotli:jvmbrotli:0.2.0")
    implementation("com.nixxcode.jvmbrotli:jvmbrotli-linux-x86-amd64:0.2.0")
    implementation("com.nixxcode.jvmbrotli:jvmbrotli-darwin-x86-amd64:0.2.0")
    implementation("com.nixxcode.jvmbrotli:jvmbrotli-win32-x86-amd64:0.2.0")
    testImplementation("junit:junit:4.12")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
//        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.dokka {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    classifier = "javadoc"
    from(tasks.dokka)
}

val sourcesJar by tasks.creating(Jar::class) {
    classifier = "sources"
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["java"])
            artifact(dokkaJar)
            artifact(sourcesJar)
        }
    }
}

bintray {
    user = project.findProperty("bintrayUser") as String?
    key = project.findProperty("bintrayKey") as String?
    setPublications("default")

    pkg(delegateClosureOf<BintrayExtension.PackageConfig> {
        repo = "openchargingnetwork"
        name = "notary"
    })
}